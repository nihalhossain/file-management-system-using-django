from django.urls import path 
from . import views


urlpatterns = [
    path('homepage', views.index, name='home'),
    path('logout', views.logoutpage, name='logout'),
    path('register', views.registerpage, name='register'),
    path('', views.loginpage, name='login'),
    path('newfolder', views.getFolderName, name='mfolder'),
   path('user-folders', views.makeFolder, name='folders'),
]