from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import UserFolder

class UserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']


class NewFolderForm(forms.ModelForm):
    class Meta:
        model = UserFolder 
        fields = ['folderName']

    widgets = {
         'folderName': forms.Textarea(attrs={'class': 'field', 'placeholder': 'folder name...'}),
         }




