from django.contrib import admin
from .models import UserFolder, Files

admin.site.register(UserFolder)
admin.site.register(Files)
