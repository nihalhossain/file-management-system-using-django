from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.http import HttpResponse
from .forms import UserForm, NewFolderForm
from django.forms import inlineformset_factory
from django.contrib.auth.decorators import login_required
from .models import UserFolder, Files


def index(request):
    if request.user.is_authenticated:
        return render(request, 'home.html')
    else:
        return redirect('login')


def registerpage(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        form = UserForm()
        if request.method == 'POST':
            form = UserForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data.get('username')
                messages.success(
                    request, 'Account created successfully for ' + username)
                return redirect('login')
    context = {'form': form}
    return render(request, 'register.html', context)


def loginpage(request):
    if request.method == 'POST':
        name = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=name, password=password)
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            messages.info(request, 'incorrect information')
    return render(request, 'login.html')


def logoutpage(request):
    logout(request)
    return redirect('home')


@login_required(login_url='login')
def getFolderName(request):
       return render(request, 'makefolder.html')


@login_required(login_url='login')
def makeFolder(request):
   folder_name = request.POST.get('folder-name')
   user = request.user.id 

   newfolder = UserFolder.objects.create(folderName=folder_name, folder_owner_id=user)
   newfolder.save() 
   folders = UserFolder.objects.filter(folder_owner_id = user)
   context = {'folders' : folders}
   return render(request, 'folders.html', context)